// Playground - noun: a place where people can play

import UIKit

func searchNames (#name: String) -> (found: Bool,description: String) {
    let names = ["a","b","c","d","e"]
    
    var found = (false,"\(name) is not a Treehouse Teacher")
    
    for n in names {
        if n == name {
            found = (true,"\(name) is a Treehouse Teacher")
        }
    }
    return found
}

let (found,description) = searchNames(name: "x")

let result = searchNames(name: "a")

result.found
result.description



