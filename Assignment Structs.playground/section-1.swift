// Assignment - Enums and Structs

import UIKit

enum Status: String {
    case Doing = "Doing"
    case Pending = "Pending"
    case Completed = "Completed"
    
    init() {
        self = .Pending
    }
}

struct Task {
    var description: String
    var status = Status()
    
    init(description: String) {
        self.description = description
    }
}

var task = Task(description: "Have Fun!")

task.status = .Completed

