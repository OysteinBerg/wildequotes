// Days in week

import UIKit

enum Day {
    case Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday
}

let days = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]

func weekdayOrWeekend(dayOfWeek: Day) -> String {
    switch dayOfWeek {
    case .Monday, Day.Tuesday, Day.Wednesday, Day.Thursday, Day.Friday:
              return "It´s a weekday"
    case Day.Saturday, Day.Sunday:
            return "Yay! It´s the weekend!"
    default:
            return "Not a valid day"
    }
}

var today = Day.Monday
today = Day.Sunday


weekdayOrWeekend(Day.Monday)
weekdayOrWeekend(Day.Saturday)


