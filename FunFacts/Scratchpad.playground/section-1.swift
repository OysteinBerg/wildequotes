// Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

// Swift Recap

var someVariable = "aVariable"

let someConstant = 20

someVariable = "anotherString"


// var fruitsArray: [String] = ["apples"]

var fruitsArray = ["apples"]
fruitsArray.append("bananas")
fruitsArray

// Optionals

var optionalString: String? = "hello"
optionalString = nil


// random number generation

var randomNumber = Int(arc4random_uniform(10))

Int(randomNumber)


// UIColor

var redColor = UIColor(red: 223/255.0, green: 86/255.0, blue: 94/255.0, alpha: 1.0)

let colorsArray = [redColor]


"A man can be happy with any woman as long as he does not love her","A man is very apt to complain of the ingratitude of those who have risen far above him","Always forgive your enemies; nothing annoys them so much","America had often been discovered before Columbus, but it had always been hushed up","America is the only country that went from barbarism to decadence without civilization in between","Anyone who lives within their means suffers from a lack of imagination","Arguments are to be avoided; they are always vulgar and often convincing","At twilight, nature is not without loveliness, though perhaps its chief use is to illustrate quotations from the poets","Biography lends to death a new terror","Consistency is the last refuge of the unimaginative","Every portrait that is painted with feeling is a portrait of the artist, not of the sitter","Fashion is a form of ugliness so intolerable that we have to alter it every six months","Genius is born--not paid","I always like to know everything about my new friends, and nothing about my old ones","I am not young enough to know everything","I think that God in creating Man somewhat overestimated his ability","I was working on the proof of one of my poems all the morning, and took out a comma","If you want to tell people the truth, make them laugh, otherwise they'll kill you","Illusion is the first of all pleasures","It is a very sad thing that nowadays there is so little useless information",







